package engineering.software.cardprototype;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;

public class CardDrawableView extends View {
    private ShapeDrawable mDrawable;
    private int mX, mY, mW, mH;
    private int mXDelta, mYDelta;
    private boolean mMoveOp;

    public CardDrawableView(Context context) {
        super(context);

        mX = 10;
        mY = 10;
        mW = 200;
        mH = 300;

        mXDelta = 0;
        mYDelta = 0;

        mMoveOp = false;

        mDrawable = new ShapeDrawable(new RectShape());
    }

    private boolean cardSelected(int x, int y) {
        return (x > mX && x < mX+mW && y > mY && y < mY+mH);
    }

    protected void onDraw(Canvas canvas) {
        mDrawable.getPaint().setColor(Color.BLACK);
        mDrawable.setBounds(mX, mY, mX + mW, mY + mH);
        mDrawable.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //run check for card selection
                //if selected, next click indicates drop off point.
                if (mMoveOp) {
                    mX = (int) e.getX() - mXDelta;
                    mY = (int) e.getY() - mYDelta;
                    mMoveOp = false;
                }
                else if(cardSelected((int)e.getX(),(int)e.getY())) {
                    mMoveOp = true;
                    mXDelta = (int) e.getX() - mX;
                    mYDelta = (int) e.getY() - mY;
                }
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }
}
